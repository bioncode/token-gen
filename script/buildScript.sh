#!/bin/sh
cd "$(dirname "$0")" || exit
cd ..
# build the binary
docker run -it --rm --name my-maven-project \
  -v maven-repo:/root/.m2 \
  -v "$(pwd)":/usr/src/mymaven \
  -w /usr/src/mymaven \
  maven:3.8.6-eclipse-temurin-17 \
  mvn clean compile package -Dmaven.test.skip

# prepare the docker image
docker build . -t gen-token-sample:1.1

# tag the image
docker tag gen-token-sample:1.1 registry.t11.caas.gcisdctr.hksarg:30058/gen-token-sample:1.1

# push to registry
docker push registry.t11.caas.gcisdctr.hksarg:30058/gen-token-sample:1.1

