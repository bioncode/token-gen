FROM eclipse-temurin:17-jre@sha256:30e722f4c370e126849a9422e4935899ad55ef0252112453e3b89d11be613bad
RUN apt-get update && apt-get install net-tools
RUN mkdir /opt/app
COPY target/gen-token-sample-1.0.1.jar /opt/app
CMD ["java", "-jar", "/opt/app/gen-token-sample-1.0.1.jar"]
